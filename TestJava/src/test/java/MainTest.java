import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.Main;

public class MainTest {

	private static Map<Character, Character> brakets = new HashMap<>();
	
	
	@BeforeClass
	public static void setUp() {
		brakets.put(')', '(');
		brakets.put('}', '{');
		brakets.put(']', '[');
		brakets.put('>', '<');
	}
	
	@Test
	public void whenInputTheBalanceString_thenIsBalanceMethod_shouldReturnTrue() {
		String balanceString = "{[()]}";
		assertTrue(Main.isBalance(balanceString));
	}
	
	@Test
	public void whenInputTheLongBalanceString_thenIsBalanceMethod_shouldReturnTrue() {
		String balanceString = "{{[[(())]]}}";
		assertTrue(Main.isBalance(balanceString));
	}
	
	@Test
	public void whenInputInvalidTheBalanceString_thenIsBalanceMethod_shouldReturnTrue() {
		String doNotBalanceString = "{[(])}";
		assertFalse(Main.isBalance(doNotBalanceString));
	}
	
	@Test
	public void whenInputTheBalanceStringList_thenVerifyMethod_shouldReturnListSize() {
		List<String> theBalanceStringList = new ArrayList<>(Arrays.asList("{[()]}", "{[()[]()]}", "{{[[(())]]}}"));
		int sizeList = theBalanceStringList.size();
		assertEquals(sizeList, Main.verify(theBalanceStringList));
	}
	
	@Test
	public void whenInputInvalidTheBalanceStringList_thenVerifyMethod_shouldReturnZero() {
		List<String> theBalanceStringList = new ArrayList<>(Arrays.asList("{[(])}", "{[(])}", "{[(])}"));
		int expect = 0;
		assertEquals(expect, Main.verify(theBalanceStringList));
	}
	
	@Test
	public void whenInputStringListWithOneElementInvalidBalanceString_thenVerifyMethod_shouldReturnListSize() {
		List<String> theBalanceStringList = new ArrayList<>(Arrays.asList("{[()]}", "{[()[]()]}", "{{[[(())]]}}", "{[(])}"));
		int expect = theBalanceStringList.size() - 1 ;
		assertEquals(expect, Main.verify(theBalanceStringList));
	}
	
}
