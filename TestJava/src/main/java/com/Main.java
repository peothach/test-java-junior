package com;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Main {
	
	private static Map<Character, Character> brakets = new HashMap<>();
	
	static {
		brakets.put(')', '(');
		brakets.put('}', '{');
		brakets.put(']', '[');
		brakets.put('>', '<');		
	}

	
	public static void main(String[] args) {
	       String[] testInput = new String[] {"{[()]}", "{[()[]()]}", "{{[[(())]]}}", "{[(])}"};
	       System.out.println(String.format("Tổng số chuỗi cân bằng là %d", verify(Arrays.asList(testInput))));
	   }

	   /**
	    * @param input Danh sách các chuỗi dấu ngoặc cần verify
	    * @return Số chuỗi cân bằng tìm thấy từ input
	    */
	   public static int verify(List<String> input) {
		   int count = 0;
		   for(String str: input) {
			   if(isBalance(str)) {
				   count++;
			   }
		   }
	       return count;
	   }
	   
	   public static boolean isBalance(String str) {
		   if (str.length() % 2 != 0) {
			   return false;
		   }
		   
		   Deque<Character> deque = new ArrayDeque<>();
		   for (char charElement: str.toCharArray()) {	
			   if(!brakets.containsKey(charElement)) {
				   deque.addLast(charElement);
				   continue;
			   }
			   
			   if(deque.isEmpty()) {
				   return false; 
			   }
			   
			   if(!Objects.equals(brakets.get(charElement), deque.peekLast())) {
				   return false;
			   }else {
				   deque.pollLast();				   
			   }	   
		   }
		   
		   return deque.isEmpty();
	   }
}
